package org.example;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.*;
import javax.mail.util.ByteArrayDataSource;

import com.sun.mail.iap.ByteArray;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);


    private static final String _EMAIL_HOST = "email-smtp.us-east-1.amazonaws.com";
    private static final String DEFAULT_SMTP_USERNAME = "AKIAT7NVYR4LAISHS3FS";
    private static final String DEFAULT_SMTP_PASSWORD = "BABsGDATTX45JPpZOWzEBDHc/1DYoeK/OeErypxXIDlg";
    static final int PORT = 25;

//    private static final String CONN_STRING = "jdbc:mysql://qa-middleware-aurora-cluster.cluster-cilmzvtievp0.us-east-1.rds.amazonaws.com";
    private static final String CONN_STRING = "jdbc:mysql://dev-middleware-aurora-cluster.cluster-cilmzvtievp0.us-east-1.rds.amazonaws.com";

    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        logger.info("Starting Standalone email application");
        Class.forName("com.mysql.cj.jdbc.Driver");
//        try ( Connection con = DriverManager
//                .getConnection("jdbc:mysql://qa-middleware-aurora-cluster.cluster-cilmzvtievp0.us-east-1.rds.amazonaws.com", "MiddlewareAdmin", "vfJ64q75ULt2")) {
        //Workaround for Validation: Connect to Dev Database
        try ( Connection con = DriverManager
                .getConnection("jdbc:mysql://dev-middleware-aurora-cluster.cluster-cilmzvtievp0.us-east-1.rds.amazonaws.com", "MiddlewareAdmin", "Q2QKm9kyMBUY42Oo4uvVolDX2h1x9c1b")) {
            while(true) {
                Thread.sleep(30000);
                new Main().deliverEmails(con);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void deliverEmails(Connection connection) {
        PreparedStatement preparedStatement = null;
        PreparedStatement removeEmail = null;
        try {
            logger.info("Run deliverMails");
            System.out.println("Running e-mail delivery");
            logger.error("Email delivery running");
            preparedStatement =
                    connection.prepareStatement("SELECT * FROM Middleware_General_Prod.email_delivery_queue");
            ResultSet rs = preparedStatement.executeQuery();
            boolean foundData = false;
            List<Integer> emailsDelivered = new ArrayList<Integer>(100);
            while (rs.next() && emailsDelivered.size() <= 100) {
                foundData = true;
                String subject = rs.getString("subject");
                String emailRecipients = rs.getString("email_recipients");
                String fromAddress = rs.getString("from_address");
                String attachmentName = rs.getString("attachment_name");
                String attachment_mime = rs.getString("attachment_mime");
                int useHtml = rs.getInt("use_html");
                Blob body = rs.getBlob("body");
                Blob attachment = rs.getBlob("attachment");
                byte[] attachmentByteArray = null;
                //NOTE: NULL ATTACHMENT HANDLING (INCOMPLETE)
//                if(!(attachment.length() == 0)) {
//                    attachmentByteArray = attachment.getBytes(1,(int) attachment.length());
//                }
//                else {
//                    attachmentByteArray = new byte[10];
//                }
                try {
                    sendEmail(subject, body.getBytes(1, (int) body.length()), false, fromAddress,
                            DEFAULT_SMTP_USERNAME, DEFAULT_SMTP_PASSWORD, emailRecipients, attachmentByteArray, attachmentName, attachment_mime);
                    emailsDelivered.add(rs.getInt("id_"));
                } catch (Throwable t) {
                    logger.error("Error delivering e-mail", t);
                }
            }
            if (foundData && !emailsDelivered.isEmpty()) {
                removeEmail = connection
                        .prepareStatement("DELETE FROM Middleware_General_Prod.email_delivery_queue WHERE id_ = ?");
                for (Integer idToRemove : emailsDelivered) {
                    removeEmail.clearParameters();
                    removeEmail.setInt(1, idToRemove);
                    removeEmail.addBatch();
                }
                removeEmail.executeBatch();
                removeEmail.close();
            }
            if(!foundData) {
                logger.info("Data in Email Queue Table not found.");
            }
            logger.info("DeliverMail execution Completed");

        }
        catch(Exception e) {
            logger.error(e);
            e.printStackTrace();
        } finally {
            try {
                if(preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (Throwable t) {
                logger.error("Error closing prepared statement", t);
            }
            try {
                if(removeEmail != null) {
                    removeEmail.close();
                }
            } catch (Throwable t) {
                logger.error("Error closing prepared statement", t);
            }
        }
    }

    /**
     * Sends Healthmap email with provided from address.
     * <p>
     * If bodyAsHTML is true, then the message will be sent as HTML instead of plain text,
     * but HTML tags such as <div> and <span> will need to be in the 'body' for it to display as HTML.
     * <p>
     * The toAddresses string is a comma delimited list of addresses to send the message.
     * <p>
     * The smtpPassword is the password of the account sending the message.
     * <p>
     * Attachment is an optional field, however, if not null, attachmentMime and attachmentName should be specified
     *
     * @param subject
     * @param body
     * @param bodyAsHTML
     * @param fromAddress
     * @param smtpPassword
     * @param toAddresses
     * @param attachment
     * @param attachmentName
     * @param attachmentMime
     * @throws MessagingException
     */
    private void sendEmail(
            String subject,
            byte[] body,
            boolean bodyAsHTML,
            final String fromAddress,
            final String smtpUsername,
            final String smtpPassword,
            String toAddresses,
            byte[] attachment,
            String attachmentName,
            String attachmentMime
    ) throws MessagingException {
        Properties properties = getHealthmapOutgoingEmailProperties();

        // Creating session and setting password
        Session session = Session.getDefaultInstance(properties);

        // Creating message and adding recipients
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromAddress));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(substituteAddresses(toAddresses)));

        // Setting subject and body of message
        message.setSubject(subject);

        if(attachment == null) {
            if (bodyAsHTML) {
                message.setContent(new String(body), "text/html; charset=utf-8");
            } else {
                message.setText(new String(body));
            }
        } else {
            Multipart multipart = new MimeMultipart();

            MimeBodyPart emailBody = new MimeBodyPart();
            emailBody.setText(new String(body));
            multipart.addBodyPart(emailBody);

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            ByteArrayDataSource source = new ByteArrayDataSource(attachment, attachmentMime);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(attachmentName);
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);
        }

        // Sending email
        Transport transport = session.getTransport();

        try {
            transport.connect(_EMAIL_HOST, smtpUsername, smtpPassword);
            transport.sendMessage(message, message.getAllRecipients());
            System.out.printf("Email Sent: To-> %s, From -> %s, subject -> %s", toAddresses, fromAddress, Arrays.toString(body));
        } catch (MessagingException ex) {
            throw ex;
        } finally {
            // Close and terminate the connection.
            transport.close();
        }
    }

    private static Properties getHealthmapOutgoingEmailProperties() {
        Properties properties = getBasicHealthmapEmailProperties();
        properties.put("mail.smtp.port", PORT);
        return properties;
    }

    private static Properties getBasicHealthmapEmailProperties() {
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.host", _EMAIL_HOST);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.protocols", "TLSv1.2");

        return properties;
    }

    public String substituteAddresses(String addresses) {
        StringBuilder sb = new StringBuilder();
        String[] toAddressAsList = addresses.split(",");
        for (String address : toAddressAsList) {
            if (!address.contains("@")) {
                try (Connection con = DriverManager
                        .getConnection("jdbc:mysql://qa-middleware-aurora-cluster.cluster-cilmzvtievp0.us-east-1.rds.amazonaws.com", "MiddlewareAdmin", "vfJ64q75ULt2")) {
                    PreparedStatement preparedStatement =
                            con.prepareStatement("SELECT * FROM Middleware_General_Prod.email_delivery_recipients WHERE `key`=?");
                    preparedStatement.setString(1, address.trim());
                    ResultSet rs = preparedStatement.executeQuery();
                    while (rs.next()) {
                        String emailList = rs.getString("email_list");
                        sb.append(emailList);
                        sb.append(",");
                    }
                }
                catch(Exception e) {
                    logger.error(e);
                    e.printStackTrace();
                }
            }
            else {
                sb.append(address);
                sb.append(",");
            }
        }
        //Note: this remove the last ','
        return sb.toString().substring(0,sb.toString().length()-1);
    }
}